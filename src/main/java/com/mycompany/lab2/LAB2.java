/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class LAB2 {
    
    static char Player = 'X';
    static char[][] Table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static int Row , Col;
    
    static void PrintWelcome(){
        System.out.println("Welcome To XO Game");
    }
    
    static void PrintTurn(){
        System.out.println("The Player " + Player + " Turn");
    }
    
    static void ShowTable(){
        for(int i = 0;i<3;i++){
                for(int j = 0;j<3;j++){
                    System.out.print("\t" + Table[i][j]);
                }
                System.out.println();
        }
        
        /* System.out.println("- - -");
        System.out.println("- - -");
        System.out.println("- - -"); */
    }
   
    static void InputRowCol(){
        Scanner kb = new Scanner(System.in);
        while(true){
            System.out.print("Please input Row Col : " );
            Row = kb.nextInt();
            Col = kb.nextInt();
            if(Table[Row-1][Col-1] == '-'){
                Table[Row-1][Col-1] = Player;
                break;
            } 
        }
    }
    
    static void SwicthPlayer(){
        if(Player == 'X'){
            Player = 'O';
        } else {
            Player = 'X';
        }
    }
    
    /*public static boolean CheckWin() {
        return CheckRow() || CheckCol() || checkDiagwin();
    }*/
    
    static boolean isWin(){
        while(true){
            if(CheckRow()){
            return true;
            }if(CheckCol()){
            return true;
            }if(checkDiagonalWin()){
            return true;
            }
            return false;
        }
    }
    
    /*static boolean CheckRow(){
        for(int i=0; i<3; i++){
            if (Table[Row-1][i] != Player) {
                return false;
            }
        }
        return true;
    }
    
    static boolean checkDiagwin(){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                if (Table[Row-1][Col-1] != Player){
                return false;
                }
            }
        }
        return true;
    }*/
    
    static boolean CheckRow(){
        for(int i=0; i<3; i++){
            if (Table[i][0] != '-' && Table[i][0] == Table[i][1] && Table[i][1] == Table[i][2]) {
                return true; 
            }
        }return false;
        
    }
    
    static boolean CheckCol(){
        for(int i=0; i<3; i++){
            if (Table[0][i] != '-' && Table[0][i] == Table[1][i] && Table[1][i] == Table[2][i]) {
                return true;
            }
        }return false;
    }
    
    static boolean checkDiagonalWin(){
        return (Table[0][0] != '-' && Table[0][0] == Table[1][1] && Table[1][1] == Table[2][2])
                || (Table[0][2] != '-' && Table[0][2] == Table[1][1] && Table[1][1] == Table[2][0]);
    }

    
    /*static boolean CheckCol(){
        for(int j=0; j<3; j++){
            if (Table[0][j] != '-' && Table[0][j] == Table[1][j] && Table[1][j] == Table[2][j]) {  
                return false;
            }
        }
        return true;
    }
     
    /*static boolean checkDiagwin(){
        if(Table[0][0] != ' ' && Table[0][0] == Table[1][1] && Table[1][1] == Table[2][2] || Table[0][2] != ' ' && Table[0][2] == Table[1][1] && Table[1][1] == Table[2][0]){
            return false;
        }
            return true; 
    }*/
 
    static boolean isDraw(){
        return true;
    }
    
    static void PrintWin(){
        System.out.println(Player + " is Winer!!!");
    }

    public static void main(String[] args) {
        PrintWelcome();
        
        while(true){
            PrintTurn();
            ShowTable();
            InputRowCol();
            if(isWin()){
                ShowTable();
                PrintWin();
                //break;
                continue;
            }
            if(isDraw()){
               ShowTable();
            }
            
            SwicthPlayer();
        }
      
            
    }
}
